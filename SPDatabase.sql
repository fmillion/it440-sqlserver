-- SP Database SQL Server Script
-- SQL Server 2012+

-- Notice: This script DELETES any tables you have in the [SP] schema.
-- If you have any data in such tables, please do not run this script unless you
--     have stored any important data in a backup somewhere!

-- You can run this script to reset your SP database to the "original" state.

-- Create the SP schema if it does not already exist.
-- A 'CREATE SCHEMA' command must be in its own command batch, so we work around this
--     by using the EXEC function, which executes a string of SQL directly in a single 
--     batch.
IF NOT EXISTS (SELECT [name] FROM [sys].[schemas] WHERE [name] = N'SPDB')
BEGIN
    EXEC('CREATE SCHEMA [SPDB] AUTHORIZATION [dbo]');
END
GO

-- This variable will hold any other raw SQL we must execute.
DECLARE @sql NVARCHAR(MAX) = N'';

-- Remove any foreign keys from tables in the SP schema.
SELECT @sql += N'ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id))
    + '.' + QUOTENAME(OBJECT_NAME(parent_object_id)) + 
    ' DROP CONSTRAINT ' + QUOTENAME([foreign_keys].[name]) + ';'
FROM [sys].[foreign_keys]
JOIN [sys].[schemas] ON [foreign_keys].[schema_id] = [schemas].[schema_id]
WHERE [schemas].[name] = 'SPDB'
EXEC sp_executesql @sql;
GO

-- Remove any tables from the SP schema.
DECLARE @sql NVARCHAR(MAX) = N'';
SELECT @sql += N'DROP TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(object_id))
    + '.' + QUOTENAME(OBJECT_NAME(object_id)) + ';'
FROM [sys].[tables]
JOIN [sys].[schemas] ON [tables].[schema_id] = [schemas].[schema_id]
WHERE [schemas].[name] = 'SPDB'
EXEC sp_executesql @sql;
GO

-- Create new tables 
CREATE TABLE [SPDB].[S] (
	[ID] VARCHAR(2) NOT NULL,
    [NAME] VARCHAR(5),
	[STATUS] INT,
    [CITY] VARCHAR(6),
	CONSTRAINT PK_S PRIMARY KEY (ID)
)

CREATE TABLE [SPDB].[P] (
    [ID] VARCHAR(2) NOT NULL,
	[PNAME] VARCHAR(5),
	[COLOR] VARCHAR(5),
	[WEIGHT] INT,
	[CITY] VARCHAR(6),
	CONSTRAINT PK_P PRIMARY KEY (ID)
)

CREATE TABLE [SPDB].[SP] (
	[SID] VARCHAR(2) NOT NULL,
	[PID] VARCHAR(2) NOT NULL,
	[QTY] INT,
	CONSTRAINT FK_SP_S FOREIGN KEY ([SID]) REFERENCES [SPDB].[S]([ID]) ON UPDATE CASCADE,
	CONSTRAINT FK_SP_P FOREIGN KEY ([PID]) REFERENCES [SPDB].[P]([ID]) ON UPDATE CASCADE,
)
GO

-- Insert test data
SET NOCOUNT ON
GO

INSERT INTO [SPDB].[S] ([ID],[NAME],[STATUS],[CITY]) VALUES('S1','Smith',20,'London');
INSERT INTO [SPDB].[S] ([ID],[NAME],[STATUS],[CITY]) VALUES('S2','Jones',10,'Paris');
INSERT INTO [SPDB].[S] ([ID],[NAME],[STATUS],[CITY]) VALUES('S3','Black',30,'Paris');
INSERT INTO [SPDB].[S] ([ID],[NAME],[STATUS],[CITY]) VALUES('S4','Clark',20,'London');
INSERT INTO [SPDB].[S] ([ID],[NAME],[STATUS],[CITY]) VALUES('S5','Adams',30,'Athens');
INSERT INTO [SPDB].[P] ([ID],[PNAME],[COLOR],[WEIGHT],[CITY]) VALUES('P1','Nut','Red',12,'London');
INSERT INTO [SPDB].[P] ([ID],[PNAME],[COLOR],[WEIGHT],[CITY]) VALUES('P2','Bolt','Green',17,'Paris');
INSERT INTO [SPDB].[P] ([ID],[PNAME],[COLOR],[WEIGHT],[CITY]) VALUES('P3','Screw','Blue',17,'Rom');
INSERT INTO [SPDB].[P] ([ID],[PNAME],[COLOR],[WEIGHT],[CITY]) VALUES('P4','Screw','Red',14,'London');
INSERT INTO [SPDB].[P] ([ID],[PNAME],[COLOR],[WEIGHT],[CITY]) VALUES('P5','Cam','Blue',12,'Paris');
INSERT INTO [SPDB].[P] ([ID],[PNAME],[COLOR],[WEIGHT],[CITY]) VALUES('P6','Cog','Red',19,'London');
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S1','P1',300);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S1','P2',200);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S1','P3',400);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S1','P4',200);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S1','P5',100);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S1','P6',100);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S2','P1',300);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S2','P2',400);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S3','P2',200);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S4','P2',200);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S4','P4',300);
INSERT INTO [SPDB].[SP] ([SID],[PID],[QTY]) VALUES('S4','P5',400);
PRINT 'SP Database tables installed.'
GO

-- End of script
