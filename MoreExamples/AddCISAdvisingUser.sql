DECLARE @starId CHAR(8) = 'PUT THE STARID HERE'

SET NOCOUNT ON

INSERT INTO [cisadvising].[account] VALUES(@starID,'','',0)
EXEC [cisadvising].[setNameForStarId] @starId
INSERT INTO [cisadvising].[assignment] VALUES(@starId,0,GETDATE(),NULL,0,0)

SELECT FirstName, LastName FROM [cisadvising].[account] WHERE starId = @starId