/* Seed Script for B507 C# Developer Workshop - Database */

-- Please read the comments in this script to understand what is going on!

USE [master]

-- If you intend to use a different database name, you must change the name in BOTH of the following statements!
CREATE DATABASE [Bwitter]
GO
USE [Bwitter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create a user-defined function to determine if a user account currently holds administrative status.
CREATE FUNCTION [dbo].[IsAdmin]
	(@handle varchar(64))
	RETURNS BIT
AS BEGIN

	-- look for user handle
	DECLARE @id INT;
	SELECT @id = [Id] FROM [User] WHERE [Handle] = @handle;
	If @id IS NULL RETURN 0
	
	-- validate user
	DECLARE @result INT
	SELECT @result = [IsAdmin] FROM [User] WHERE [Id] = @id
	IF @result = 1 RETURN 1 
	RETURN 0

END
GO

-- Create a user-defined function to determine if a user and password combination is valid.
CREATE FUNCTION [dbo].[ValidateUser] 
	(@handle varchar(64), @password varchar(max))
	RETURNS BIT
AS BEGIN

	-- look for user handle
	DECLARE @id INT;
	SELECT @id = [Id] FROM [User] WHERE [Handle] = @handle;
	If @id IS NULL RETURN 0
	
	-- validate user
	DECLARE @storedHash BINARY(32)
	SELECT @storedHash = [Key] FROM [User] WHERE [Id] = @id
	DECLARE @providedHash BINARY(32) = HASHBYTES('SHA2_256',@password)	
	IF @storedHash = @providedHash RETURN 1 
	RETURN 0

END
GO

-- Create a table to hold user accounts.
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [binary](32) NOT NULL,
	[Handle] [varchar](32) NOT NULL,
	[FullName] [varchar](128) NOT NULL,
	[Bio] [varchar](140) NULL,
	[IsAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

-- Create a table to hold user "tweets" (messages)
CREATE TABLE [dbo].[Message](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Time] [datetime] NOT NULL,
	[Message] [varchar](140) NOT NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Insert some seed data into the User and Message tables.
SET IDENTITY_INSERT [dbo].[User] ON 
GO
INSERT [dbo].[User] ([Id], [Key], [Handle], [FullName], [Bio], [IsAdmin]) VALUES (1, 0x5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8, N'admin', N'Administrator', N'This is the default administrator account.', 1)
INSERT [dbo].[User] ([Id], [Key], [Handle], [FullName], [Bio], [IsAdmin]) VALUES (2, 0x04F8996DA763B7A969B1028EE3007569EAF3A635486DDAB211D512C85B9DF8FB, N'user', N'First User', N'This is your very first Bwitter user!', 0)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO

SET IDENTITY_INSERT [dbo].[Message] ON 
GO
INSERT [dbo].[Message] ([Id], [User], [Time], [Message]) VALUES (1, 2, CAST(N'2018-01-23 15:49:30.380' AS DateTime), N'My first B507 Message!')
GO
SET IDENTITY_INSERT [dbo].[Message] OFF
GO

-- Add the foreign key constraint for the Message table.
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_User]
GO

-- Add a stored procedure to make it simple to add new messages given a user handle and a message.
CREATE PROCEDURE [dbo].[AddMessage] 
	@UserHandle varchar(32),
	@Message varchar(140)
AS
BEGIN
	SET NOCOUNT ON;
	-- look for user handle
	DECLARE @id INT;
	SELECT @id = [Id] FROM [User] WHERE [Handle] = @UserHandle;
	If @id IS NULL RAISERROR('Handle not found',11,1)
	-- insert message
	INSERT INTO [Message] ([User],[Time],[Message]) VALUES(@id,GETDATE(),@message)
END

